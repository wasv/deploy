# Docker-Compose S2I Deploy Scripts for Git

## Setup
- Install docker
- Install docker-compose
- Create a network for nginx-proxy (if needed)
  - `docker network create bridge_nginx`
- Make a directory named `git` in your home directory.
  - `mkdir ~/git`
- Clone this repo into `git/deploy`
  - `git clone [this repo] ~/git/deploy`

## Adding a Project
- `git init --bare ~/git/[project].git`
- Create a directory under `deploy`
  - `mkdir ~/git/deploy/[project]`
  - Add a `docker-compose.yml` file (and any supporting files).
- Link the appropriate deploy script as a git hook.
  - `ln -s $HOME/git/deploy/scipts/[name].sh ~/git/[project].git/hooks/post-update`
