#/bin/bash

set -e

name="$1" # Ex. decisive.git
if test "${name}" = ""; then
    name=$(basename $(pwd))
fi
project=$(basename ${name} ".git") # Ex. decisive

basedir="$HOME/git/"
gitdir="${basedir}${name}" # Ex. ~/git/decisive.git
configdir="${basedir}deploy/config/${project}" # Ex. ~/git/deploy/config/decisive
proxydir="${basedir}deploy/config/proxy" # Ex. ~/git/deploy/config/nginx
workdir="/tmp/${project}_deploy" # Ex. /tmp/decisive_deploy

# Build container in workdir.
mkdir -p "${workdir}"
git --git-dir=${gitdir} --work-tree=${workdir} checkout --force master
docker build -f ${configdir}/Dockerfile -t ${project} ${workdir}
#rm -rf ${workdir}

# Start services.
cd ${configdir} && docker-compose up -d
cd ${proxydir} && docker-compose up -d
