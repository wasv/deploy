#/bin/bash

set -e

BUILD_IMAGE=centos/python-36-centos7

name="$1" # Ex. decisive.git
if test "${name}" = ""; then
    name=$(basename $(pwd))
fi
project=$(basename ${name} ".git") # Ex. decisive

basedir="$HOME/git/"
gitdir="${basedir}${name}" # Ex. ~/git/decisive.git
configdir="${basedir}deploy/config/${project}" # Ex. ~/git/deploy/config/decisive
proxydir="${basedir}deploy/config/proxy" # Ex. ~/git/deploy/config/nginx
workdir="/tmp/${project}_deploy" # Ex. /tmp/decisive_deploy

# Build container in workdir.
mkdir -p "${workdir}"
git --git-dir=${gitdir} --work-tree=${workdir} checkout --force master
s2i build -q ${workdir} ${BUILD_IMAGE} ${project}
rm -rf ${workdir}

# Start services.
cd ${configdir} && docker-compose up -d
cd ${proxydir} && docker-compose up -d
